#include <stdio.h>
#include <stdlib.h>


struct Node{
 int num;
 int i;
 struct Node *n;
}; 
typedef struct Node no;

int tam;

void inicia(no *LISTA);
char menu(void);
void opcao(no *LISTA, char opcao );
void insereFim(no *LISTA);
void insereInicio(no *LISTA);
void mostra(no *LISTA);
void apaga(no *LISTA);
void add (no *LISTA);



int main(void)
{
 no *LISTA = (no *) malloc(sizeof(no));
 if(!LISTA){
  printf("Sem memoria disponivel!\n");
  exit(1);
 }else{
 inicia(LISTA);
 char lista;
 
 do{
  lista=menu();
  opcao(LISTA,lista);
 }while(lista);

 free(LISTA);
 return 0;
 }
}

void inicia(no *LISTA)
{
 LISTA->n = NULL;
 tam=0;
}

int vazia(no *LISTA)
{
 if(LISTA->n == NULL)
  return 1;
 else
  return 0;
}

no *guarda()
{
 no *new=(no *) malloc(sizeof(no));
 if(!new){
  printf("Sem memoria disponivel!\n");
  exit(1);
 }else{
  printf("digite o valor "); 
  scanf("%d", &new->num);
  printf("adicionado com sucesso\n\n");
  return new;
 }
}


void insereFim(no *LISTA)
{
 no *new=guarda();
 new->n = NULL;
 
 if(vazia(LISTA))
  LISTA->n=new;
 else{
  no *tmp = LISTA->n;
  
  while(tmp->n != NULL)
   tmp = tmp->n;
  
  tmp->n = new;
 }
 tam++;
}

void insereInicio(no *LISTA)
{
 no *new=guarda(); 
 no *oldHead = LISTA->n;
 
 LISTA->n = new;
 new->n = oldHead;
 
 tam++;
}

void mostra(no *LISTA)
{

 if(vazia(LISTA)){
 	printf("_______________");
	printf("\n| Lista vazia! |\n");
	printf("_______________ \n");
	system("pause");
  return ;
 }
 
 no *tmp;
 tmp = LISTA->n;
 printf("_____________________");
	printf("\n| Lista  | \n");
	
 while( tmp != NULL){
  printf("| %5d  | \n", tmp->num);
  tmp = tmp->n;
 }
 printf("_____________________\n");
 system("pause");
}

void apaga(no *LISTA)
{
 if(!vazia(LISTA)){
  no *proxNode,
     *atual;
  
  atual = LISTA->n;
  while(atual != NULL){
   proxNode = atual->n;
   free(atual);
   atual = proxNode;
  }
 }}
char menu(void)
{
char lista;

 printf("digite a opcao para escolhe-la \n");
		printf("__________________________________\n");
		printf("| OPCAO                 |  NOME  | \n");
		printf("|--------------------------------| \n");
		printf("| 0 SAIR                | S      | \n");
		printf("| 1 EXIBIR LISTA        | E      | \n");
		printf("| 2 ADICIONAR NO INICIO | I      | \n");
		printf("| 3 ADICIONAR NO FINAL  | F      | \n");
		printf("| 4 ZERAR LISTA         | Z      | \n");
		printf("|________________________________|\n");		
		printf("\n\n digite o nome da opcao :  "); scanf("%c", &lista);
 
 printf("\n");
 return lista;
}

void opcao(no *LISTA, char opcao )
{
 no *tmp;
 switch(opcao ){
 	
  case 's':
  	system("cls");
   apaga(LISTA);
   printf("\n____________\n");
   printf("| Encerrado |");
   printf("\n|___________|\n");
   system("pause");
   break;
   
  case 'e':
  	system("cls");
	mostra(LISTA);
   break;
  
  case 'i':
  	system("cls");
	insereInicio(LISTA);
   break;
  
  case 'f':
  	system("cls");
  insereFim(LISTA);
   break;  
   
  case 'z':
  	system("cls");
   apaga(LISTA);
   inicia(LISTA);
   printf("\n_______________\n");
   printf("| Lista Zerada |");
   printf("\n|______________|\n");
   system("pause");
   break;

  
 }
}



